# eureka-feign

#### 项目介绍
Feign是一个声明式的Web服务客户端，使用Feign可使得Web服务客户端的写入更加方便。 
它具有可插拔注释支持，包括Feign注解和JAX-RS注解、Feign还支持可插拔编码器和解码器、Spring Cloud增加了对Spring MVC注释的支持，并HttpMessageConverters在Spring Web中使用了默认使用的相同方式。Spring Cloud集成了Ribbon和Eureka，在使用Feign时提供负载平衡的http客户端。

#### 软件架构
service-registry 注册中心<br>
service-user 用户服务<br>
service-order 订单服务<br>

### 运行流程
1. 运行注册中心
2. 开启用户服务
3. 开启订单服务
4. 打开订单服务，访问/hello路由
5. 就会显示hello,doaio!


