package com.doaio.serviceuser.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @RequestMapping("/getName")
    public String getName(){
        return "doaio";
    }
    @RequestMapping("/getNameHystrix")
    public String getNameHystrix(){
        try {
            Thread.sleep(2000);  //Hystrix默认超时时1000毫秒，超时会自动调用降级处理helloFallback()
        }catch (Exception e){
//            e.printStackTrace();
        }
        return "getNameHystrix";
    }
    @RequestMapping("/")
    public String index(){
        return "i am service-user";
    }
}
