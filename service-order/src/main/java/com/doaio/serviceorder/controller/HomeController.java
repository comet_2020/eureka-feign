package com.doaio.serviceorder.controller;

import com.doaio.serviceorder.feign.UserFeignApi;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Autowired
    private UserFeignApi userFeignApi;

    @RequestMapping("/hello")
    @HystrixCommand(fallbackMethod = "seyHello")
    public String hello() {
        try {
            Thread.sleep(2000);  //Hystrix默认超时时1000毫秒，超时会自动调用降级处理helloFallback()
        } catch (Exception e) {
//            e.printStackTrace();
        }
        String name = userFeignApi.getName();
        return "hello," + name + "!";
    }

    @RequestMapping("/hello2")
    @HystrixCommand(fallbackMethod = "seyHello")
    public String hello2() {
        String name = userFeignApi.getName();
        return "hello," + name + "!";
    }

    /**
     * Hystrix 以类的形式进行服务降级
     *
     * @return cc
     */
    @RequestMapping("/helloHystrix")
    public String helloHystrix() {
        String name = userFeignApi.getNameHystrix();
        return "hello," + name + "!";
    }

    public String seyHello() {

        return "服务降级,友好提示！";
    }

    @RequestMapping("/")
    public String index() {
        return "i am service-order";
    }

}
