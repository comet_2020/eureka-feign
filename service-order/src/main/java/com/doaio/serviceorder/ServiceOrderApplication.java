package com.doaio.serviceorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
//@EnableDiscoveryClient   //如果选用的注册中心是eureka，那么就推荐@EnableEurekaClient，如果是其他的注册中心，那么推荐使用@EnableDiscoveryClient。
@EnableFeignClients
@EnableHystrix //开启 Hystrix
//@EnableCircuitBreaker // 开启熔断器       //@EnableHystrix与@EnableCircuitBreaker的区别:@EnableHystrix继承了@EnableCricuitBreaker
public class ServiceOrderApplication {


    public static void main(String[] args) {
        SpringApplication.run(ServiceOrderApplication.class, args);
    }
}
