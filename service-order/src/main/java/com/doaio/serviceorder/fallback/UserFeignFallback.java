package com.doaio.serviceorder.fallback;

import com.doaio.serviceorder.feign.UserFeignApi;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author xie.shilin@qq.com
 * @version V1.0
 * @Package com.doaio.serviceorder.fallback
 * @date 2021/3/5 15:23
 */
@Component //Hystrix 以类的形式进行服务降级
public class UserFeignFallback implements UserFeignApi {

    @Override
    public String getName() {
        return "服务器忙， 请稍后重试！";
    }

    @Override
    public String getNameHystrix() {
        return "getNameHystrix服务器忙， 请稍后重试！";
    }
}
