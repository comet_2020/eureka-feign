package com.doaio.serviceorder.feign;

import com.doaio.serviceorder.fallback.UserFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "service-user",fallback = UserFeignFallback.class) //Hystrix 以类的形式进行服务降级
//@FeignClient(value = "service-user")
public interface UserFeignApi {
    @RequestMapping("/getName")
    String getName();

    @RequestMapping("/getNameHystrix")
    String getNameHystrix();
}
